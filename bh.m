function [dQdb1, A, AP] = bh(betaz)
load 'hw3.mat'
dQdb1 = zeros(1, 6);
for i = 1:601
dQdb = - (exp(X(i , :)*betaz)).*X(i, :) + y(i).*X(i , :);

dQdb1 = dQdb1 + dQdb;
end

AP = zeros(6,6);
for i = 1:601
   A_k = ((-(exp(X(i, :)*betaz)).*X(i, :) + y(i)*X(i, :))')*(- (exp(X(i, :)*betaz)).*X(i, :) + y(i).*X(i, :));
   AP = AP + A_k; %Hessian
end

A = pinv(AP);
end
function y = secant2(q, a, b, tol)

c = (a*q(b) - b*q(a))/(q(b)-q(a));
y = c;
end
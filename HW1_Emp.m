%Question 1
X = [1 1.5 3 4 5 7 9 10];

Y1 = [];
Y1 = -2+0.5*X;

Y2 = [];
Y2 = -2 + 0.5*(X.^2);

figure
plot(X, Y1, X, Y2)
title('Q1')
xlabel('X')
ylabel('Y')
legend('Y1','Y2');

%Question 2

Y = linspace(-10, 20, 200);
sum_Y = sum(Y);

%Question 3

A = [2 4 6; 1 7 5; 3 12 4];
B = [-2; 3; 10];

C = (A.')*B;
% inv is slow. see the faster alternative in the answer key
D = inv((A.')*A)*B;

E =0;
for i = 1:3
    for j = 1:3
      K = A(i,j)*B(i);
      E = E + K;
    end
end

row = [2];
column = [3];
F = A;
F(row, :) = [];
F(:, column) = [];


x = linsolve(A,B);

%Question 4
% see how to use kron()
Block_B = blkdiag(A,A,A,A,A);

%Question 5

Norm_A = normrnd(10, 5, [5, 3]);
% there is much faster way to do it.
for i = 1:5
    for j = 1:3
        if Norm_A(i,j) < 10
            Norm_A(i,j) = 0;
        else
            Norm_A(i,j) = 1;
        end
    end
end

%Question 6
% nest time assume that hte file is in the same directory. I have different
% paths on my computer
filename = 'datahw1.csv';
delimiter = ',';

formatSpec = '%f%f%f%f%f%f%[^\n\r]';

fileID = fopen(filename,'r');

dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
fclose(fileID);
Firm_Id = dataArray{:, 1};
Year = dataArray{:, 2};
Export = dataArray{:, 3};
RD = dataArray{:, 4};
Prod = dataArray{:, 5};
Cap = dataArray{:, 6};
clearvars filename delimiter formatSpec fileID dataArray ans;

XR = [Export, RD, Cap];
beta = regress (Prod, XR);

lm = fitlm(XR, Prod);
% could have used beta = lm.Coefficients
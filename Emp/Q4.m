clc;clear; 
load 'hw4data.mat'
 global randomvariable
 global X N T Y Z

 X = (data.X);
 N = (data.N);
 T = data.T;
 Y = (data.Y);
 Z = (data.Z);
%  

%% %%%%%%%%Question 4


sigma2 = 1;
beta0 = 0.1;
gamma = 0;
u0 =0.1;
sigmau=1;
sigmacov=0.1;


rng(74323);
randomvariable = randn(100000,2);
corrcoef(randomvariable);
% why 1e6 points? this is takes forever in matlab. only use this many
% points if you write program in C++ or you know how to write fast acting
% code in matlab.

Var =[sigma2^0.5  sigmacov
    sigmacov  sigmau^0.5  ];

C = chol(Var);
randomvariableu = randomvariable * C;
corrcoef(randomvariableu);
randomvariableu(:,1) = beta0+randomvariableu(:,1);
randomvariableu(:,2) = u0+randomvariableu(:,2);
corrcoef(randomvariableu);


lowerbound = [  -3  0   -3  -2  0   -1];
upperbound = [  5   5   3   2   3   1];
% why are you imposing boounds? there was nothing in the assignment about
% bounds, the problem is convex, you will find the solution always with
% simple newtons method.

parameter =[0.1,    1,  0,  0.1,    1,  0.1];

[estimate,FunValue]  = fmincon(@(p) MonteCarlo(p),parameter,[],[],[],[],lowerbound,upperbound)

% this did not converge on my computer for 5 minutes, means it is
% inefficient at the very least.
function lnL=MonteCarlo(P)
global N T X Y Z
global randomvariable
iteration = 1000;
beta0 = P(1);
sigmabeta = P(2);
gamma = P(3);
mu0 = P(4);
sigmau = P(5);
sigmacov = P(6);

M =[sigmabeta^0.5  sigmacov
    sigmacov  sigmau^0.5  ];
C = chol(M);
randomvar = randomvariable * C;
randomvar(:,1) = beta0+randomvar(:,1);
randomvar(:,2) = mu0+randomvar(:,2);

randbta=reshape(randomvar(:,1),[N iteration]);
randmu=reshape(randomvar(:,2),[N iteration]);

likelihood = ones(N,iteration);


for iN = 1: N
    for iiteration = 1: iteration
        for iT=1:T
            epi= randbta(iN,iiteration)*X(iT,iN)+gamma*Z(iT,iN)+randmu(iN,iiteration);
            t1 = exp(-epi);
            likelihood(iN,iiteration) = likelihood(iN,iiteration)*(1/(1+t1))^Y(iT,iN)*...
                (t1/(1+t1))^(1-Y(iT,iN));
        end
    end
end

Likelihood1 = log(sum(likelihood,2)/iteration);
lnL    = -sum(Likelihood1);

end
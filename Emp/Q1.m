clc;clear; 
load 'hw4data.mat'
 
 global X N T Y Z

 X = (data.X)';
 N = (data.N)';
 T = data.T;
 Y = (data.Y)';
 Z = (data.Z)';
 
[beta, w] = GaussHermite_2(20);

fun1 = ones(100,20);
fun3 = zeros(100,1);
fun5 = 1;
for i = 1:N
for j = 1:20
for t = 1:T
fun(i,j,t) = ((1/(1+exp(-(X(i,t)*(beta(j)*sqrt(2) + 0.1)))))^Y(i,t))*((1/(1+exp(X(i,t)*(beta(j)*sqrt(2) + 0.1))))^(1-Y(i,t)))*(1/sqrt(3.14));
fun1(i,j) = fun1(i,j).*fun(i,j,t);
end
fun2(i,j)= fun1(i,j).*w(j);
fun3(i) = fun3(i) + fun2(i,j);
end
fun4(i) = log(fun3(i));
fun5 = fun5 + fun4(i);
end

%%%%%%%Question 2

delta = normrnd(0.1, 1, [100, 1]);

gg1 = ones(100,100);
gg2 = zeros(100,1);
gg5 = 1;
for i = 1:N
   for j = 1:100
      for t = 1:T
      gg(i,j,t) = ((1/(1+exp(-(X(i,t)*delta(j)))))^Y(i,t))*((1/(1+exp(X(i,t)*delta(j))))^(1-Y(i,t)));
      gg1(i,j) = gg1(i,j).*gg(i,j,t);
      end
     gg2(i) = gg2(i) + gg1(i,j);
   end
   gg3(i) = gg2(i)/100;
   gg4(i) = log(gg3(i));
   gg5 = gg5 + gg4(i);
end

%%%%%%%%Question 3

paisa = @maximum
gamma = fminsearch(paisa, [0.1; 1; 0]);
hello = @monte
A = [0, -1, 0];
b =0
gamma1 = fmincon(hello, [0.1; 1; 0],A, b);



function gg6 = likelihood(p)
rng default
load 'hw4data.mat'
mu = [p(1); p(2)];
Sigma = [p(3), p(4); p(4), p(5)];

 X = (data.X)';
 N = (data.N)';
 T = data.T;
 Y = (data.Y)';
 Z = (data.Z)';
 
delta = mvnrnd(mu, Sigma, 100);

gg1 = ones(100,100);
gg2 = zeros(100,1);
gg5 = 1;
for i = 1:N
   for j = 1:100
      for t = 1:T
      gg(i,j,t) = ((1/(1+exp(-(X(i,t)*delta(j,1)+ Z(i,t)*p(6)+ delta(j,2)))))^Y(i,t))*((1/(1+exp(X(i,t)*delta(j,1)+ Z(i,t)*p(6)+delta(j,2))))^(1-Y(i,t)));
      gg1(i,j) = gg1(i,j).*gg(i,j,t);
      end
     gg2(i) = gg2(i) + gg1(i,j);
   end
   gg3(i) = gg2(i)/100;
   gg4(i) = log(gg3(i));
   gg5 = gg5 + gg4(i);
end
gg6 = -gg5;
end
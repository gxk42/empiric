 load ('hw4data.mat')

 %%%% Question 1
 [beta, w] = GaussHermite_2(20);
 X = (data.X)';
 N = (data.N)';
 T = data.T;
 Y = (data.Y)';
 Z = (data.Z)';

fun1 = 1;
fun3 = 0;
fun4 = 1;
for i = 1:N
for j = 1:20
for t = 1:T
fun = ((1/(1+exp(-(X(i,t)*(beta(j)*sqrt(2) + 0.1)))))^Y(i,t))*((1/(1+exp(X(i,t)*(beta(j)*sqrt(2) + 0.1))))^(1-Y(i,t)))*(1/sqrt(3.14));
fun1 = fun1*fun;
end
fun2 = fun1*w(j);
fun3 = fun3 + fun2;
end
 fun4 = fun4*fun3;
end
% You calculate likelihood, not log likelihood. it creates severe problems
% of underflow. Never calculate likelihood, always calculate loglikelihood.





%%%% Question 2

delta = normrnd(0.1, 1, [100, 1]);

gg1 = 1;
gg2 = 0;
gg4 = 1;
for i = 1:N
   for j = 1:100
      for t = 1:T
      gg = ((1/(1+exp(-(X(i,t)*delta(j)))))^Y(i,t))*((1/(1+exp(X(i,t)*delta(j))))^(1-Y(i,t)));
      gg1 = gg1*gg;
      end
     gg2 = gg2 + gg1;
   end
   gg3 = gg2/100;
   gg4 = gg4*gg3;
end
% same problem here

delta = fminunc(gg4, 0.1);
% fminunc needs to receive function as first argument, not a variable.


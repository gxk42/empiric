function fun6 = maximum(gamma)
 load 'hw4data.mat'

 X = (data.X)';
 N = (data.N)';
 T = data.T;
 Y = (data.Y)';
 Z = (data.Z)';
 
[beta, w] = GaussHermite_2(20);
 
fun1 = ones(100,20);
fun3 = zeros(100,1);
fun5 = 1;
for i = 1:N
for j = 1:20
for t = 1:T
fun(i,j,t) = ((1/(1+exp(-(X(i,t)*(beta(j)*sqrt(2)*gamma(2) + gamma(1)) + Z(i,t)*gamma(3)))))^Y(i,t))*((1/(1+exp(X(i,t)*(beta(j)*sqrt(2)*gamma(2) + gamma(1))+ Z(i,t)*gamma(3))))^(1-Y(i,t)))*(1/sqrt(3.14));
fun1(i,j) = fun1(i,j).*fun(i,j,t);
end
fun2(i,j)= fun1(i,j).*w(j);
fun3(i) = fun3(i) + fun2(i,j);
end
fun4(i) = log(fun3(i));
fun5 = fun5 + fun4(i);
end

fun6 = -fun5;
end
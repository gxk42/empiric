function gg6 = monte(gamma)
rng default
 load 'hw4data.mat'

 X = (data.X)';
 N = (data.N)';
 T = data.T;
 Y = (data.Y)';
 Z = (data.Z)';
 mu = gamma(1);
 sigma = gamma(2);
delta = normrnd(gamma(1), gamma(2), [100, 1]);

gg1 = ones(100,100);
gg2 = zeros(100,1);
gg5 = 1;
for i = 1:N
   for j = 1:100
      for t = 1:T
      gg(i,j,t) = ((1/(1+exp(-(X(i,t)*delta(j)+ Z(i,t)*gamma(3)))))^Y(i,t))*((1/(1+exp(X(i,t)*delta(j)+ Z(i,t)*gamma(3))))^(1-Y(i,t)));
      gg1(i,j) = gg1(i,j).*gg(i,j,t);
      end
     gg2(i) = gg2(i) + gg1(i,j);
   end
   gg3(i) = gg2(i)/100;
   gg4(i) = log(gg3(i));
   gg5 = gg5 + gg4(i);
end
gg6 = -gg5;
end
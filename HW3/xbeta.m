function xbeta = xbeta(betaz)
% there is no hw3.mat in this directory, either include it or navigate to
% it 
load '~/Google Drive/econ_512_2017/homework/hw3.mat'
for i = 1:601
    hello(i) = exp(X(i, :)*betaz) - y(i)*X(i , :)*betaz + log ((factorial(y(i))));
end
    
xbeta = sum(hello);
end
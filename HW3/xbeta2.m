function [f, g] = xbeta2(betaz)
load '~/Google Drive/econ_512_2017/homework/hw3.mat'
for i = 1:601
    hello(i) = exp(X(i, :)*betaz) - y(i)*X(i , :)*betaz + log ((factorial(y(i))));
    uggs(i, :) = (exp(X(i, :)*betaz))*X(i, :) - y(i)*X(i, :);
end
f = sum(hello);
g = sum(uggs);
end
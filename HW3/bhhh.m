function [beta, AP] = bhhh(beta1, tol)

[dQdb1, A, AP] = bh(beta1);
beta2 = beta1 + A*(dQdb1');
k = beta2 - beta1

while norm(k) > tol
    beta1 = beta2;
    [dQdb1, A, AP] = bh(beta1);
    beta2 = beta1 + A*(dQdb1');
    k = beta2 - beta1;
end

beta = beta1;

end
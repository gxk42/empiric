function xbeta3 = xbeta3(betaz)
load '~/Google Drive/econ_512_2017/homework/hw3.mat'
for i = 1:601
    hello(i) = - exp(X(i, :)*betaz) + y(i)*X(i , :)*betaz - log ((factorial(y(i))));
end
    
xbeta3 = sum(hello);
end
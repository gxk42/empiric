function [theta] = Newton (Likelihood, Likelihood_D, theta1, tol)
    
theta2 = theta1 - Likelihood(theta1)/Likelihood_D(theta1);

k = theta2 - theta1;

while abs(k) > tol
    theta1 = theta2;
    theta2 = theta1 - Likelihood(theta1)/Likelihood_D(theta1);
k = theta2 - theta1;

end

theta = theta1;
end
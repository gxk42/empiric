
%%%%%%%%%%%%%%%%%%% Question 1 part c
n = 1000;
thet1 = 1;
thet2 = 1;
for i = 1:1000
x = gamrnd(thet1,thet2,[n 1]);
Y1 = mean(x);
Y2 = exp(sum(log(x))/n);

Likelihood = @(theta1) sum(log(x))/n - psi(theta1) + log (theta1) - log(Y1);
Likelihood_D = @(theta1) -trigamma(theta1) + 1/theta1;
% this is inefficient, you calculate mean log and log mean every time you
% calculate likelihood and derivative

theta_1 (i)= Newton(Likelihood, Likelihood_D, 1, 0.001);
 theta_2(i) = theta_1(i)/Y1;

cov_var = [-n*trigamma(thet1) , n/thet2 ; n/thet2 , -n*thet1/thet2^2];
 
 Var_Cov = -pinv(cov_var);
 
 y1(i) = Y1/Y2;
end
  plot(y1, theta_1)
%% %%%%%%%%%%%%%%%%% Question 2

%%%%%%Part 1
  Beta1 = fminunc(@xbeta, [0;0;0;0;0;0] );
  
  %%%%%%%%%%%%%%%%%%%%
  options = optimoptions('fminunc','Algorithm','trust-region','SpecifyObjectiveGradient',true);
  Beta2 = fminunc(@xbeta2,[0;0;0;0;0;0],options);
  %%%%%%%%%%%%%%%%%%%%
    [Beta3, S] = neldmead(@xbeta3, [0;0;0;0;0;0]);
    %%%%%%%%%%
 [beta, AP] = bhhh( [0;0;0;0;0;0], 0.001);
 %%%%%%%%%%%%% Part 2
 A = eig(AP);
 %%%%%%%%%%%%% Part 3
 BetaNlls = fminunc(@xbeta4, [0;0;0;0;0;0]);
 
 %%%%%%%%%%%% Part 4
 Cov_Var_BHHH = pinv(AP);
 G = diag(Cov_Var_BHHH);
 SE_BHHH = sqrt(G)/sqrt(601);
 
 %%%%%%%%%% Variance of NLLS Method
 load '~/Google Drive/econ_512_2017/homework/hw3.mat'
 D = zeros(6,6);
 V = 0;
 for i = 1:601
     D = D + (X(i, :)'.*exp(X(i, :)*BetaNlls)^2.*X(i, :));
     V = V + (y(i) - exp(X(i, :)*BetaNlls))^2.*(X(i, :)'.*exp(X(i,:)*BetaNlls)^2.*X(i, :));
 end
 V = 4/601*V;
 D = 1/601*D;
 psi = D\V/D;
 Sou = pinv(psi);
 Bab = diag(Sou);
 SE_NLLS = sqrt(Bab)/sqrt(601);
 
 
  
  
  
  
  
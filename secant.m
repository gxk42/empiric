function y = secant(q, a, b, tol)

c = (a*q(b) - b*q(a))/(q(b)-q(a));

while abs (q(c)) > tol
    
    a = b; b = c;
    c = (a*q(b) - b*q(a))/(q(b)-q(a));
end 

y = c;
end
%%Question 1
qa = @(v_a, v_b, v_c, p_a, p_b, p_c)exp(v_a - p_a)/(1+ exp(v_a - p_a) + exp(v_b - p_b) + exp(v_c - p_c));
qb = @(v_a, v_b, v_c, p_a, p_b, p_c)exp(v_b - p_b)/(1+ exp(v_a - p_a) + exp(v_b - p_b) + exp(v_c - p_c));
qc = @(v_a, v_b, v_c, p_a, p_b, p_c)exp(v_c - p_c)/(1+ exp(v_a - p_a) + exp(v_b - p_b) + exp(v_c - p_c));
 
 qa(-1, -1, -1, 1, 1, 1)
 qb(-1, -1, -1, 1, 1, 1);
 qc(-1, -1, -1, 1, 1, 1);

%%Question 2
 fval =@(p) [1 - p(1)*(1-(exp(-1 - p(1))/(1+ exp(-1 - p(1)) + exp(-1 - p(2)) + exp(-1 - p(3))))); 
             1 - p(2)*(1-(exp(-1 - p(2))/(1+ exp(-1 - p(1)) + exp(-1 - p(2)) + exp(-1 - p(3)))));
             1 - p(3)*(1-(exp(-1 - p(3))/(1+ exp(-1 - p(1)) + exp(-1 - p(2)) + exp(-1 - p(3)))))]


tic 
x = broyden (fval, [1;1;1]);  
% get the other output of the broyden
toc
tic
y = broyden (fval, [0;0;0]); 
toc 
tic
z = broyden (fval, [0;1;2]);
toc
tic
q = broyden (fval, [3;2;1]);
toc
%%Question 3

w = [1, 0 , 0, 3;1, 0, 1, 2;1, 0, 2, 1];

d = zeros(3,4);
for j = 1:4
    f = 1;
    tic
while f > 0.0001
q_a = @(w1)(1 - w1*(1-(exp(-1 - w1)/(1+ exp(-1 - w1) + exp(-1 - w(2,j)) + exp(-1 - w(3,j))))));
q_b = @(w2)(1 - w2*(1-(exp(-1 - w2)/(1+ exp(-1 - w(1,j)) + exp(-1 - w2) + exp(-1 - w(3,j))))));
q_c = @(w3)(1 - w3*(1-(exp(-1 - w3)/(1+ exp(-1 - w(1,j)) + exp(-1 - w(2,j)) + exp(-1 - w3)))));
wj(1,j) = secant(q_a, w(1,j), w(1,j)+1, 0.001);
wj(2,j) = secant(q_b, w(2,j), w(2,j)+1, 0.001);
wj(3,j) = secant(q_c, w(3,j), w(3,j)+1, 0.001);
% Here you also need to extract more information of the optimization run.
% see the answer key

f = max(abs(wj(:,j) - w(:,j)));

w(1,j) = wj(1,j);
w(2,j) = wj(2,j);
w(3,j) = wj(3,j);
end
toc
d(1,j) = w(1,j);
d(2,j) = w(2,j);
d(3,j) = w(3,j);
end

%%Question 4

t = [1, 0 , 0, 3;1, 0, 1, 2;1, 0, 2, 1];
D4 = zeros(3,4);
op = zeros(3,1);
for j = 1:4 
k = 1;
tic
   while k> 0.0001
op = 1./([1;1;1] - [(exp(-1 - t(1,j))/(1+ exp(-1 - t(1,j)) + exp(-1 - t(2,j)) + exp(-1 - t(3,j)))); 
                    (exp(-1 - t(2,j))/(1+ exp(-1 - t(1,j)) + exp(-1 - t(2,j)) + exp(-1 - t(3,j))));
                    (exp(-1 - t(3,j))/(1+ exp(-1 - t(1,j)) + exp(-1 - t(2,j)) + exp(-1 - t(3,j))))]);
 
 k = max(abs(op - t(:,j)));
t(1,j) = op(1);
t(2,j) = op(2);
t(3,j) = op(3);
   end
toc   
D4(1,j) = t(1,j);
D4(2,j) = t(2,j);
D4(3,j) = t(3,j);
end

%%Question 5 

w= [1, 0 , 0, 3;1, 0, 1, 2;1, 0, 2, 1];

d5 = zeros(3,4);
iter = zeros(4,1);
for j = 1:4
  f = 1;
tic
  while f > 0.0001
q_a = @(w1)(1 - w1*(1-(exp(-1 - w1)/(1+ exp(-1 - w1) + exp(-1 - w(2,j)) + exp(-1 - w(3,j))))));
q_b = @(w2)(1 - w2*(1-(exp(-1 - w2)/(1+ exp(-1 - w(1,j)) + exp(-1 - w2) + exp(-1 - w(3,j))))));
q_c = @(w3)(1 - w3*(1-(exp(-1 - w3)/(1+ exp(-1 - w(1,j)) + exp(-1 - w(2,j)) + exp(-1 - w3)))));
wj(1,j) = secant2(q_a, w(1,j), w(1,j)+1, 0.001);
wj(2,j) = secant2(q_b, w(2,j), w(2,j)+1, 0.001);
wj(3,j) = secant2(q_c, w(3,j), w(3,j)+1, 0.001);

f = max(abs(wj(:,j) - w(:,j)));

w(1,j) = wj(1,j);
w(2,j) = wj(2,j);
w(3,j) = wj(3,j);
iter(j) = iter(j) + 1;
  end
  toc
  iter(j) = iter(j);
  d5(1,j) = w(1,j);
d5(2,j) = w(2,j);
d5(3,j) = w(3,j);
end
